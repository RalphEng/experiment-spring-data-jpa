Experiment Spring-Data-JPA
==========================

# Lombok
This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

## Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
>An unexpected exception occured while performing the refactoring.
>
>Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536

# Resources
- [Reference Documentation](https://docs.spring.io/spring-data/jdbc/docs/1.1.4.RELEASE/reference/html/#reference)
- [github](https://github.com/spring-projects/spring-data-jdbc) with some examples and documentation
- Blog Article 1: [Introducing Spring Data JDBC](https://spring.io/blog/2018/09/17/introducing-spring-data-jdbc)
- Blog Article 2: [Spring Data JDBC, References, and Aggregates](https://spring.io/blog/2018/09/24/spring-data-jdbc-references-and-aggregates)
