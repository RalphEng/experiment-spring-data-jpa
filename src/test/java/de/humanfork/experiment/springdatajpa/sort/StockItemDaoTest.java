package de.humanfork.experiment.springdatajpa.sort;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;

import de.humanfork.experiment.springdatajpa.sort.StockItem;
import de.humanfork.experiment.springdatajpa.sort.StockItemDao;

@DataJpaTest
public class StockItemDaoTest {

    @Autowired
    private StockItemDao stockItemDao;

    @PersistenceContext
    private EntityManager entityManager;

    private void buildScenario() {
        this.entityManager.persist(new StockItem("B", "Name B", "Titel B", "Le Name B"));
        this.entityManager.persist(new StockItem("A", "Name A", "Titel A", "Le Name A"));

        this.entityManager.persist(new StockItem("C", "Name C", null, null));
        this.entityManager.persist(new StockItem("D", null, "Titel D", null));
        this.entityManager.persist(new StockItem("E", null, "", "Le Name E"));

        this.entityManager.flush();
        this.entityManager.clear();
    }

    @DataJpaTest
    @Nested
    public class SortTest {

        @Test
        public void testSimpleSort() {
            buildScenario();

            List<StockItem> findAll = stockItemDao.findAll(StockItemDao.nameEnNotEmpty, Sort.by("nameEn"));
            assertThat(findAll).extracting(StockItem::getKey).containsExactly("A", "B", "C");
        }

        @Test
        public void testSortTwoAttributes() {
            buildScenario();

            List<StockItem> findAll = stockItemDao.findAll(Sort.by("nameEn", "key"));

            //Nulls are ordered first, so D, E become the first items
            assertThat(findAll).extracting(StockItem::getKey).containsExactly("D", "E", "A", "B", "C");
        }
    }

    @DataJpaTest
    @Nested
    public class JpaSortUnsafeTest {

        @Test
        public void testJpaSortTwoAttributes() {
            buildScenario();

            List<StockItem> findAll = stockItemDao.findAll(JpaSort.unsafe("nameEn", "key"));

            //Nulls are ordered first, so D, E become the first items
            assertThat(findAll).extracting(StockItem::getKey).containsExactly("D", "E", "A", "B", "C");
        }

        @Test
        public void testJpaSortCoalesce() {
            buildScenario();

            List<StockItem> result = stockItemDao.findStockItems(
                    JpaSort.unsafe("COALESCE(nameEn, nameDe, nameFr)"));

            assertThat(result).extracting(StockItem::getKey).containsExactly("E", "A", "B", "C", "D");
        }
    }

    @DataJpaTest
    @Nested
    public class SortBySpecification {
        @Test
        public void testSort() {
            buildScenario();

            List<StockItem> result = stockItemDao.findAll(StockItemDao.nameEnNotEmpty.and(StockItemDao.orderByName));

            //Nulls are ordered first, so D, E become the first items
            assertThat(result).extracting(StockItem::getKey).containsExactly("A", "B", "C");
        }
        
        @Test
        public void testLimit() {
            buildScenario();

            Page<StockItem> result = stockItemDao.findAll(StockItemDao.nameEnNotEmpty.and(StockItemDao.orderByName),
                    PageRequest.of(0, 2)
                    );

            result.getContent().forEach(System.out::println);
            
            //Nulls are ordered first, so D, E become the first items
            assertThat(result.getContent()).extracting(StockItem::getKey).containsExactly("A", "B");
            assertThat(result.getTotalElements()).isEqualTo(3);
        }
    }

}
