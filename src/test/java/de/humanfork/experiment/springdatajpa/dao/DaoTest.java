package de.humanfork.experiment.springdatajpa.dao;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import de.humanfork.experiment.springdatajpa.fetchgraph.Cart;
import de.humanfork.experiment.springdatajpa.fetchgraph.Customer;
import de.humanfork.experiment.springdatajpa.fetchgraph.CustomerDao;
import de.humanfork.experiment.springdatajpa.fetchgraph.Shop;

@DataJpaTest
public class DaoTest {

    @Autowired
    private CustomerDao customerDao;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testLoadNormal() {
        buildScenario();

        System.out.println("*** load Normal ****");
        System.out.println("findAll");
        Iterable<Customer> result = this.customerDao.findAllNormal();
        accessAttributes(result);
    }
    
    @Test
    public void testLoad_LoadGraph() {
        buildScenario();

        System.out.println("*** load LoadGraph ****");
        System.out.println("findAll");
        Iterable<Customer> result = this.customerDao.findAllLoad();
        accessAttributes(result);
    }
    
    @Test
    public void testLoad_FetchGraph() {
        buildScenario();

        System.out.println("*** load FetchGraph ****");
        System.out.println("findAll");
        Iterable<Customer> result = this.customerDao.findAllFetch();
        accessAttributes(result);
    }
    
    private void buildScenario() {
        Shop shopA = new Shop("A");
        this.entityManager.persist(shopA);

        Shop shopB = new Shop("B");
        this.entityManager.persist(shopB);

        Cart cartA = new Cart("A");
        this.entityManager.persist(cartA);

        Cart cartB = new Cart("B");
        this.entityManager.persist(cartB);

        Customer customerA = this.customerDao.save(new Customer("A", shopA, cartA));
        Customer customerB = this.customerDao.save(new Customer("B", shopB, cartB));

        this.entityManager.flush();
        this.entityManager.clear();
    }

    private void accessAttributes(Iterable<Customer> customers) {
        System.out.println("1. getName");
        assertThat(customers).extracting(Customer::getName).containsExactlyInAnyOrder("A", "B");
        System.out.println("2. getShop");
        assertThat(customers).extracting(c -> c.getShop().getName()).containsExactlyInAnyOrder("A", "B");
        System.out.println("3. getCart");
        assertThat(customers).extracting(c -> c.getCart().getItem()).containsExactlyInAnyOrder("A", "B");
        System.out.println("Done");
    }

 

}
