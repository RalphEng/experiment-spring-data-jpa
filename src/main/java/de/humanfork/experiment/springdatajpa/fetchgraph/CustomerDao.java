package de.humanfork.experiment.springdatajpa.fetchgraph;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;

public interface CustomerDao extends CrudRepository<Customer, Long> {

    @Query("Select c FROM Customer c")
    List<Customer> findAllNormal();
    
    /**
     * An entity graph can be used as a fetch or a load graph.
     * The attributes specified by the entity graph will be treated as FetchType.EAGER.
     * 
     * If a <b>fetch graph</b> is used: all other attributes will be lazy.
     * 
     * @return
     */
    @EntityGraph(attributePaths = {"shop"}, type = EntityGraphType.FETCH)
    @Query("Select c FROM Customer c")
    List<Customer> findAllFetch();
    
    
    /**
     * An entity graph can be used as a fetch or a load graph.
     * The attributes specified by the entity graph will be treated as FetchType.EAGER.
     * 
     * If a <b>load graph is used</b>, all attributes that are not specified by the entity graph will keep their default fetch type.
     * 
     * @return
     */
    @EntityGraph(attributePaths = {"shop", "cart"}, type = EntityGraphType.LOAD)
    @Query("Select c FROM Customer c")
    List<Customer> findAllLoad();
    
}
