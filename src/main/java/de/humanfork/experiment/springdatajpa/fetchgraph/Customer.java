package de.humanfork.experiment.springdatajpa.fetchgraph;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.ToString;

@Entity
@Getter
@ToString
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String name;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Shop shop;
    
    @OneToOne//(fetch = FetchType.LAZY)
    private Cart cart;

    protected Customer() {
    }

    public Customer(String name, Shop shop,  Cart cart) {   
        this.name = name;
        this.shop = shop;
        this.cart = cart;
    }
  
    
}