package de.humanfork.experiment.springdatajpa.sort;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface StockItemDao extends JpaRepository<StockItem, Long>, JpaSpecificationExecutor<StockItem> {

    public Specification<StockItem> nameEnNotNull = (Root<StockItem> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) -> root.get("nameEn").isNotNull();

    public static Specification<StockItem> nameEnNotEmptyString = (Root<StockItem> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) -> root.get("nameEn").isNotNull();

    public static Specification<StockItem> nameEnNotEmpty = nameEnNotNull.and(nameEnNotEmptyString);

    public static Specification<StockItem> orderByName = (Root<StockItem> root, CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder) -> {
        query.orderBy(criteriaBuilder.asc(root.get("nameEn")));
        return null;
    };

    @Query("Select si FROM StockItem si")
    public List<StockItem> findStockItems(Sort sort);

}
