package de.humanfork.experiment.springdatajpa.sort;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.lang.Nullable;

import lombok.Getter;
import lombok.ToString;

/**
 * A Stock item has a key, and at least one english, german, or french name.
 */
@Entity
@Getter
@ToString
public class StockItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String key;

    @Nullable
    private String nameEn;

    @Nullable
    private String nameDe;

    @Nullable
    private String nameFr;
    
    /** Hibernat constructor. */
    StockItem() {
        super();
    }

    public StockItem(String key, String nameEn, String nameDe, String nameFr) {
        this.key = key;
        this.nameEn = nameEn;
        this.nameDe = nameDe;
        this.nameFr = nameFr;
    }

    public String getName() {
        if (nameEn != null && !nameEn.isEmpty()) {
            return nameEn;
        }
        if (nameDe != null && !nameDe.isEmpty()) {
            return nameDe;
        }
        if (nameFr != null && !nameFr.isEmpty()) {
            return nameFr;
        }
        throw new IllegalStateException("Stock item `" + this.key + "` has no name");
    }

}
